eb 3rd 2019 Weekly Report

## AI 基准测试集仍然不够成熟

**标题**：AI Benchmarks Remain Immature

**作者**：Linley Gwennap

**机构**: The Linley Group MPR report 

**链接**: https://www.linleygroup.com/mpr/article.php?id=12093

**参考文献引用**：The Linley Group. AI Benchmarks Remain Immature: MLPerf Posts Few Results; AI-Benchmark Mixes Formats. Microprocessor report 12093. January 28, 2019.

&emsp;&emsp; 有不少流行的测试程序应用评价CPU和GPU的性能，但即使AI负载已经变得越来越普遍，比较AI性能仍然是具有挑战性的任务。许多芯片制造商仅引用每秒浮点峰值执行性能（flops/s），或者对于仅提供整数计算的设计采用每秒操作数（op/s）峰值。但类似CPU，深度学习加速器（DLAs）由于在软件、存储或者其他设计部分的可能瓶颈而经常在低于理论峰值性能的速度运行。所有人都同意性能应该以运行真实应用来衡量，但在选择哪个应用以及如何运行方面并无法达成一致。

&emsp;&emsp; 为了解决这一问题，数个领先厂商联合起来开发了一组涵盖AI训练和推理的宽泛测试程序集。MLPerf一开始作为学术界和工业界的合作项目，参与方包括了Google、Stanford、Harvard和百度。支持者的列表一直在增长，包括数据中心处理器的传统商家AMD、Intel和Nvidia，以及初创公司Cerebras、Esperanto、SambaNova和Wave；移动处理器开发商华为、联发科、高通和三星；IP供应商ARM、Cadence和Synopsys；以及Super 7云服务提供商中的5位。Amazon、Apple和Ceva是令人瞩目的缺席者。MLperf当前完成了第一组测试集发布，主要测试目标是大规模网络训练性能，Google、Intel和Nvidia给出了初始结果。

&emsp;&emsp; 针对AI的另外一面，来自ETH Zurich的研究人员，作为顶尖的技术研究大学，开发了一组测试AI在智能手机上推理性能的程序。这组测试被称为AI-Benchmark，采用小规模的神经网络，以安卓APP的方式呈现，能够产生成千上万的结果。许多测试集使用了安卓的神经网络API接口（NNAPI，发音为“nappy”）可以通过处理器开发商提供驱动支持，以从各种集成加速器中获益。因为不能在iOS上运行app，除了Apple之外所有流行的移动处理器都有测试结果。

&emsp;&emsp; 通过吸引到大量工业界的参与者，MLPerf避免了之前DeepBench的问题。DeepBench仅由百度单独构建，获得的关注非常少（参考 MPR 11/21/16，DeepBench测试深度学习）。但大团队的弊病在于测试集的酝酿期太长了。另外，初期的测试结果也很分散；仅覆盖了三个厂商的4款芯片，并且没有任何一个跑完了全部测试集。与之相反，AI-Benchmark自动运行所有测试，并且以app应用的方式发布测试集，产生的大量的结果，包括一些没有发布的产品。但是解释这些结果非常困难，因为在神秘的权重系统下混合了加速后以及未加速的测试。

### MLPerf以训练开始

&emsp;&emsp; 初始的MLPerf测试集解决数据中心的AI训练，但是整个群体也涉及开发数据中心系统中的AI推理测试，以及在移动甚至IoT设备中AI推理测试的计划。在初始结果表中的每一项都代表了一个神经网络模型，一个数据集以及一个精度目标。一次测试运行评价标准是：在数据集上训练一个模型直到其达到精度量化目标的时间。例如，图像分类测试选择v1.5版本的深度残差网络ResNet-50，作为典型的卷积神经网络，在流行的ImageNet测试集上进行训练，直到其至少能够准确分类74.9%的图像。训练时间被进一步与参考训练时间相除（从单块Nvidia P100加速卡上获得），以获得一个分数，以此来标识对参考系统的加速。

| 芯片类型 | Google TPUv2 | Google TPUv3 | Intel Xeon 8180 | Nvidia V100 |
|:-------- | :----------: | :----------: | :-------------: | :---------: |
| 图像分类 | 7.3 | 12.1 | 0.4 | 8.2 |
| 目标识别（轻量）| 2.1 | 2.8 | -- | 3.9|
| 目标识别（重载）| -- | -- | -- | 1.9 |
| 翻译（循环）| 7.0 | 10.8 | -- | 7.8 |
| 翻译（非循环）| -- | -- | -- | 7.2 | 
| 推荐 | -- | -- | 0.8 | 11.7 |
| 强化学习 | -- | -- | 3.2 | -- | 

&emsp;&emsp; 测试集中包含两个目标识别测试，都采用了CNN。轻量级版本采用了SSD(也称 ResNet-34)，而重载版本采用Mask R-CNN。同样，两种不同的模型将英语文本翻译为德语。循环的版本依据Google的神经机器翻译模型，一种循环神经网络（RNN）；而非循环的版本使用了变形体Transformer，一种基于新“自注意力”（self attention）模型的方法。推荐系统采用了另外一种神经网络：多层感知机（MLP: MultiLayer Perceptron），以神经协作过滤模型的方式构建。这一类似网飞Netflix的测试进行电影推荐。最后的测试是一个强化学习实现，运行一个Python程序以完成自我训练，目标是下围棋。

### 训练的顶尖选手

&emsp;&emsp; 自然，每一个人都希望知道哪一个处理器是对AI训练的最佳选择。MLPerf已经上传的结果仅包括：Nvidia的V100（Volta）芯片（在Nvidia公司的DGX-1和DGX-2系统上测试）；Intel最昂贵的Xeon可扩展处理器，Platinum 8180；和Google的定制TPUv2和TPUv3芯片。（TPUv1是为推理优化因而不能运行这一测试。）在这些厂商中，Nvidia运行了7个测试中的6个（以CPU为中心的强化学习在GPU上运行效率极低），而其他厂商仅运行了7个测试中的3个，并且还不是完全相同的3个。厂商也没有被要求公开系统功耗，因此也没有任何数据。

&emsp;&emsp; 厂商运行测试的系统包含不同数量的芯片，从2到260。为了简化比较，Linley团队从最少数量的芯片开始，并归一化处理以体现单个芯片的性能。虽然性能并不会线性增长，Nvidia在大多数网络上，从1个GPU到8个GPU展示了95%的线性，因此这一简化方法有基本的精确性保障。

&emsp;&emsp; 由于公布数据非常零散，非常难完成有意义的比较。比较显然的是，CPU是大多数AI训练中效率最低的平台。在Intel和Nvidia都提交结果分数的测试（图像识别与推荐系统），Nvidia单芯片是Intel的15到20倍。只能假设在其他测试上，Intel的分数更加糟糕，因而没有提交分数。同时，需要记住旗舰版本的8180至强芯片标价1万美元；主流的至强芯片（例如，Gold 5120）只有其1/4的性能，除了CPU核减少外，每个核的向量单元也减半。基于Python的强化学习展示了一种在CPU上运行良好的测试。偶尔，纯CPU训练对那些在许多标准服务器上运行各种任务的客户也是有意义的。

&emsp;&emsp; TPUv3在与Nvidia V100能比较的3个测试中有2个领先：图像识别与循环网络版文本翻译。但是Google的芯片用低精度权重目标识别模型（SSD）抄了近路。再次，人们也不得不怀疑Google提交的数据中为什么缺少了其他测试；可能TPU运行其他模型并不顺畅。即使在TPUv3上效果最好的模型（图像识别用的卷积网络）上，其比更早1年发布的V100领先也低于50%。

&emsp;&emsp; 由于缺乏厂商提供的功耗数据，只能进行功耗效率的猜测评估。V100典型功耗为300W；Google没有给出TPUv3的典型功耗，但Linley团队相信采用液冷方案的设计与V100处于相同功耗水平。因此预测两种产品具有接近的功耗效率。当然他们也都需要CPU计算介入来完成神经网络操作，但显然要比Xeon 8180的205W典型功耗要低。即使假设与GPU和TPU芯片相连的CPU功耗为100W，这些芯片也比8180功耗效率高8到10倍。

### AI-Benchmark 使用安卓的NNAPI

&emsp;&emsp; 类似MLPerf，AI-Benchmark由数个基于流行神经网络的测试组成，但主要测试评价推理而不是训练。为了聚焦于能够在智能手机上运行的模型，app应用所需要的DRAM内存少于2GB，并且通常在只有1GB内存的手机上运行。权重以及其他预先训练网络的参数消耗存储空间低于100MB；最大的Inception v3模型有96MB，但某些模型小于1MB。测试都打包在单一App内，用户可以通过安卓市场下载。在运行几分钟后，AI-Benchmark自动上传结果到官方网站，按照手机和处理器显示所有已经完成的测试。

&emsp;&emsp; 与MLPerf包含广泛的模型与应用不同，AI-Benchmark专注于基于CNN的图像处理，这在移动AI领域是最常见的使用场景。第一个测试是图像分类，但没有使用庞大的ResNet-50，而是更紧凑的MobileNet模型。测试以三种不同模式上依次运行。第一次直接在CPU上运行。第二次使用NNAPI来访问任何可用能处理浮点的硬件加速器。第三次以“规格量化”版本用8位整数来进行运算。对于实现了INT8格式的加速器，量化版本性能超过翻一番，因为同时大幅减少了存储容量和带宽需求。在官方网站上CPU、浮点和整数模式分别用C、F和Q表示。

&emsp;&emsp; 第二个测试同样是图像分类，但使用更大尺寸也更精确的Inception v3模型。这一模型在每次推理过程中约需要50亿次MAC操作，大约是MobileNet需求的10倍。如下表所示，测试集内也包括面部识别和图像去模糊。两个超解析super-resolution测试试图从低清晰度的版本中重建原始图像，即按照相似图像训练的结果来填充消失的像素。图像分割测试将整幅图像分割为类似天空、建筑和路面等基础类别。最后的图像增强测试执行例如色彩增强、锐化和去噪等图像编辑操作。下表显示了每个测试中实用的特定神经网络，以及是否仅在CPU、或者在NNAPI的FP与整形模式下运行。

| 测试任务 | 神经网络 | 测试类型 | 数据类型 |
| :------- | :------: | :------: | :------: |
| 1a目标分类| MobileNet v2 | CPU Only | FP |
| 1b目标分类| MobileNet v2 | NNAPI | FP |
| 1c目标分类| MobileNet v2 | NNAPI | INT8|
| 2目标分类| Inception v3 | NNAPI | FP |
| 3脸部识别| Inception ResNet v1| NNAPI | INT8 |
| 4图像去模糊| SRCNN 9-5-5 | NNAPI | FP |
| 5图像超解析| VGG-19 | NNAPI | INT8 |
| 6图像超解析| SRGAN | CPU Only | FP |
| 7图像分割| ICNet,2倍并行 | CPU Only | FP |
| 8图像增强| ResNet-12 | NNAPI | FP |

&emsp;&emsp; 测试推理要比训练简单，因为处理器只需要按照已经提供的权重值简单执行网络模型即可。对于INT8测试，规格量化后的权重也预先处理完成，因此所有手机都使用相同的权重数据进行计算。在理论上，所有的计算应该在所有设备上产生相同的结果，但是AI-Benchmark并不检查输出的精度。在未来的版本中会增加这一检查，因为某些硬件设计会以牺牲精度的方式来提高速度。

&emsp;&emsp; 除了单个的测试结果，AI-Benchmark会输出一个总分，这一方式产生了某些争议（MLPerf团队避免了这一问题）。AI-Score是各个测试结果的加权和，但权重仅由ETH团队确定并且不公开。另外，ETH团队在不同版本的APP中修改了权重，特定产品的评级因此而发生改变。当前的权重倾向于高FP性能的处理器，而不注重整数DLA加速器。

Linley分析师则使用无加权的所有测试几何平均来计算总分，使用骁龙820为基准。同时包括CPU、FP和整数测试的各自总分，也采用简单的几何平均。（与算术平均相比，几何平均减少了单个超高或者超低测试结果对总分的影响）。由这种方式计算的分数和AI-Score大部分是一致的，除了骁龙845上测试集underweight，而在重浮点的Kirin 970上测试集overweights。下表展示了Linley分析师给出的总分，基于AI-Benchmark官网的单个测试程序数据，按照类别区分。

| 移动处理器 | 所有测试几何平均 | CPU测试几何平均 | FP测试几何平均 | INT8测试几何平均 |
| :--------- | :--------------: | :-------------: | :------------: | :--------------: |
| Snapdragon 855 | 7.7 | 2.3 | 12.5 | 13.9 |
| Helio P90 | 7.4 | 1.6 | 8.9 | 27.6 |
| Kirin 980 | 6.3 | 2.3 | 10.4 | 8.6 |
| Snapdragon 845 | 5.2 | 1.7 | 7.7 | 9.3 |
| Helio P60 | 2.0 | 0.9 | 1.6 | 5.5 |
| Kirin 970| 1.6 | 2.3 | 12.5 | 13.9 |
| Exynos 9810| 1.3 | 1.5 | 1.3 | 1.2 |
| Exynos 8895 | 1.2 | 1.2 | 1.3 | 1.3 |
| Snapdragon 835 | 1.2 | 1.2 | 1.1 | 1.3 |


### 智能手机飙升的AI性能

&emsp;&emsp; AI-Benchmark结果上最明显的一点就是智能手机制造商增强神经网络支持的速度。最新的处理器比骁龙820的性能高6到8倍，而后者在3年前还是高端旗舰移动处理器。根据子分类数据，CPU的性能提升要缓慢许多，3年仅提高了2倍。最大的提升是在NNAPI驱动下的测试，通过使用更强力的GPU或者DLA性能提高了10倍不止。

&emsp;&emsp; 在所有已经批量出货的产品中，华为的Kirin 980压倒了骁龙845。980在CPU测试上分数更好，可能部分原因其使用了Cortex-A76而不是Cortex-A75，后者缺乏ARM最新的点积指令支持（参考 MPR 7/9/18 ARM点积加速CNN）。与Kirin 970仅具有FP加速器不同，980在INT8测试总也获得了高分，这显示新的设计也对整数计算进行加速。845依赖于其高效的Hexagon DSP来运行INT8测试，但除了GPU之外缺乏额外的FP加速器。

虽然高通的下一代骁龙855还没有真正的手机出货，但已经在AI-Benchmark数据库中出现，这归因于量产前的硬件测试。使用定制的Cortex-A76设计，855的CPU测试性能和Kirin 980一样，但GPU和DSP的性能大幅领先。高通之前承诺从845到855有3倍的AI性能提升，但初步数据显示提高只有50%。量产前版本的NNAPI驱动程序可能没有使能855最新的张量加速器，它的INT8分数应该比表中的数据更高（参考 MPR 12/24/18 骁龙855是首选）。即使如此，855也在各种分数中明确领先于当前的所有产品。

联发科的Helio P90，是另外一个量产前产品，对于一个目标是中高端市场的产品而言其分数具有压倒性意义，考虑到其定价可能低于列表中的其他产品，可能会非常畅销。P90压倒了Kirin 980而仅落后于骁龙955，抬高了AI性能的门槛。联发科用额外的硅片面积资源来获得强力的AI加速器（参考 MPR 1/28/19，Helio P90抬高了中高端门槛）。

三星的Exynos处理器在这一比较中落后，但这并不怪芯片设计师；三星公司没有提供NNAPI驱动程序，因此所有的AI-Benchmark都仅仅在CPU上运行。Exynos芯片上包含之前从DeePhi授权的DLA，但三星从来没有揭示其性能，同时测试程序也没有提供任何有效洞察。

### 不能结束的总结

&emsp;&emsp; MLPerf以正确的想法开始，联合了尽可能多的AI公司已支持一组常见的AI测试集。整个团队非常谨慎的分析可选的模型和客户需求，拉近各个成员的观点和专家意见。将这些想法落地需要较长时间，而且当前仅有的MLPerf是一个初始发布，并且在2019年底之前不会有1.0版本的发布。有众多评论家的大团队可能并非AI这个飞速发展技术领域的最佳方法。

&emsp;&emsp; AI-Benchmark选择的不同的方式，依赖于独立研究人员构成的小团队来构建测试集。这种短平快的方式使得更新非常迅速；AI-Benchmark已经到2.1版本了。例如，由于某些特定的神经网络以整数权重的方式呈现，测试集立刻采纳了这些量化模型。这一方式的缺陷是对单一小团队的观点过度依赖。例如，在当前手机的生命周期中，移动市场已经转向INT8数据类型的推理，但AI-Score给INT8测试分配的权重非常低。幸运的是，APP应用将FP和INT8的结果分开来报告，因此用户可以关注于他们认为重要的结果。

&emsp;&emsp; 随着MLPerf持续迭代演进，可能会有一些变化。但最致命的问题在于制造商可以仅提交部分测试结果。因此可以选择看起来最好的结果而掩盖不好的。强烈建议MLPerf委员会要求所有提交必须包括所有7个测试的分数。提交测试系统的功耗也非常有用，这对于数据中心运营商是重要尺度。虽然AI-Benchmark也缺乏功耗尺度，但几乎所有的智能处理器都有类似的功耗限制。

&emsp;&emsp; 测试性能是一个复杂的问题，特别是对于AI这样一个具有多个变种的年轻技术；汇总成一个分数的办法过于简化了问题现状。数据中心运营商需要最佳的硬件来支撑特定的混合模型，因此一个一个模型的数据结果是最有益的。相反的是，几乎没有智能手机用户知道他们的应用究竟是用浮点还是整数进行计算，并且即使用户知道，这些app非常可能在明年改头换面。在这一场景下，一个最终分数是有用的。但是为了保证可信度，AI-Benchmark需要对如何计算结果更加透明。 


**点评**：

&emsp;&emsp;
